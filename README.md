# Growth hacking open source contribution

## Context

Read https://gitlab.com/jibe-b/doctorat-es-cooperation

## Method

Record clics in the README using 
http://framaclic.org/

(note that javascript cannot be embedded in a markdown file served on github or
gitlab https://gitlab.com/jibe-b/test-de-javascript-dans-markdown/blob/master/README.md)

Record activity on your project page using
http://matomo.org/

Mine git logs & merge requests à la
https://2019.msrconf.org/

Make A/B testing using different branches
https://stackoverflow.com/questions/23609293/is-git-suitable-for-a-b-testing-code-swapping